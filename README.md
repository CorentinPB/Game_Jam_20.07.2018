# Game_Jam_20.07.2018

Petit jeu créé en 48h durant une game jam sur Unity. Nous étions 4 et 2 d'entres nous ne connaissions pas Unity avant de commencer (je n'en faisait pas parti).
Dans l'état actuel du jeu, nous avons un menu principal et un menu d'options (résolution, qualité et volume). Le choix de la qualité de change rien actuellement (pas le temps).
Le jeu génère un terrain de façon aléatoire avec 2 hauteurs disponibles. Le personnage a des animations de jump, idle, running, walking, wallsliding, double jump.
Le joueur peut wallslide (ralenti lors de la chute sur un mur), walljump (petit jump hors du mur avec un saut normal possible dès la sortie du walljump), courrir (touche MAJ) et double jump.
Je me suis chargé du déplacement du personnage, des graphismes (toutes les textures) et de diverseses autres tâches dans le projet.

Nous avions prévu d'ajouter plus de choses :
 - Plus d'assets dans le background.
 - Une plus grande variété dans la génération de terrain (actuellement, un peu trop simple).
 - Des pièges et des ennemies.
 - Correction des erreurs dans le comportement du personnage (quelques problèmes peuvent apparître comme par exemple des double saut qui ne trigger pas ou des animations un peu bizarre).
 - Un terrain qui peut bouger (rotation 90°) afin de transformer le scrolling horizontal en scrolling vertical (haut ou bas) afin d'exploiter la mécanique de wall jump.
 - Un menu principal un peu moins cheap.
 - Un high score.
 - Des niveaux pré-générés (afin de rendre les niveaux plus intéressant).
 

Il n'est pas impossible le jeu évolue dans le futur avec quelques améliorations.

Bugs connus :

Il existe comme je l'ai dis quelques problèmes avec le déplacement du joueur et également un problème lors du passage d'une résolution basse à une résolution plus haute (disparition du background). Le score n'affiche que 5 chiffres max donc problème lors du passage aux score 10000+.